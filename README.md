**Autors**

*Miquel Vidal*

*Jerome Lomio*

*Andrés Jordán*

**Descripció**

Basant-vos en el document html que heu fet per la pt4 de composició bàsica, ara heu de modificar el CSS per que tingui l'aparença d'una web de la Generalitat (basant-vos en la informació de la guia d'estil de la Generalitat)

Heu de fer un css pels colors, un per la tipografia, un per l'esquena (marges..) i un de genèric si el necessiteu.

HEU DE TREBALLAR AMB GIT cadascú fa un css, el puja al projecte, i els altres l'agafen del projecte. No us heu de passar fitxers per mail ni amb un pen usb ni similars

Revisaré l'historial de commits del projecte.

**Recordeu, una guia d'estil marca la coherència:**

Colors
Fonts
Esquema (retícula)
Imatges
